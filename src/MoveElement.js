const PHONE = window.navigator.userAgent.toLowerCase().match(/phone|android|ipad|ios|ipad/)
const touchstart = PHONE ? 'touchstart' : 'mousedown'
const touchmove = PHONE ? 'touchmove' : 'mousemove'
const touchend = PHONE ? 'touchend' : 'mouseup'
const touchleave = PHONE ? 'touchleave' : 'mouseleave'

export default class MoveElement {
    constructor($dom) {
        this.$ele = $dom
        this.state = {
            x: 0,
            y: 0,
            startX: 0,
            startY: 0,
            moveX: 0,
            moveY: 0,
            changeX: 0,
            changeY: 0,
            isStart: false,
        }
        this.addEvents()
    }
    destory(){
        this.removeEvents()
    }
    removeEvents(){
        $ele.removeEventListener(touchstart, this.touchstart)
        $ele.removeEventListener(touchmove, this.touchmove)
        $ele.removeEventListener(touchend, this.touchend)
        $ele.removeEventListener(touchleave, this.touchleave)
    }
    addEvents(){
        let {$ele} = this
        $ele.addEventListener(touchstart, this.touchstart)
        $ele.addEventListener(touchmove, this.touchmove)
        $ele.addEventListener(touchend, this.touchend)
        $ele.addEventListener(touchleave, this.touchleave)
    }
    updatePosition(x, y){
        this.$ele.style.cssText += `; transform: translate3d(${x}px, ${y}px, 0);`
    }
    reset(){
        let {state}= this
        state.changeX = state.changeY = state.clientX = state.clientY = state.startX = state.startY = state.moveX = state.moveY = 0
    }
    touchstart = (event) => {
        this.reset()
        let {state}= this

        let touch = event.touches ? event.touches[0] : event
        state.moveX = state.startX = touch.clientX
        state.moveY = state.startY = touch.clientY

        state.isStart = true
    }
    touchmove = (event) => {
        event.preventDefault()
        event.stopPropagation()
        let {state}= this
        if (!state.isStart) {
            return
        }

        let touch = event.touches ? event.touches[0] : event
        state.moveX = touch.clientX
        state.moveY = touch.clientY

        state.changeX = state.moveX - state.startX
        state.changeY = state.moveY - state.startY

        this.updatePosition(state.changeX + state.x, state.changeY + state.y)
    }
    touchend = (event) => {
        let {state}= this
        if (!state.isStart) {
            return
        }
        state.isStart = false

        state.x += state.changeX
        state.y += state.changeY
    }
    touchleave = event => {
        this.touchend(event)
    }
}
