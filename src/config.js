export default {
    mockApi: "//mock.weipaitang.com/", // 返回mock数据
    jsProxy: "//webpack.weipaitang.com/api/proxy/", // 静态文件请求代理，接受一个 proxy/http://xxx.ww.com/ss.js
    frpProxy: "//proxy.wpt.la/", // 内网穿透，使用示例 //proxy.wpt.com/@用户名@/xx.js
    routerPath: "/webApp/router.js.log", // 路由文件，用来覆盖支出路由
    var: "WPT", // 会在全局变量上挂载 loadScriptHost，以供后续加载
    JSHost: '172.16.20.1',
    apiProxy: [
        {
            name: 'wpt',
            test: /((.+\.)?(weipaitang|youjiang)\..+)|localhost/,
            envs: [
                { name: 'w', list: [] },
                {
                    name: 't',
                    list: [
                        ['api.youjiang.xin'],
                        ['api.weipaitang.com'],
                        ['pay.weipaitang.com'],
                        ['payapi.weipaitang.com'],
                        ['imapi.weipaitang.com'],
                        ['myapi.weipaitang.com'],
                        ['workorder.weipaitang.com'],
                        ['cfc.weipaitang.com'],
                    ]
                },
                {
                    name: 'dev',
                    list: [
                        ['api.youjiang.xin'],
                        ['api.weipaitang.com'],
                        ['pay.weipaitang.com'],
                        ['payapi.weipaitang.com'],
                        ['imapi.weipaitang.com'],
                        ['myapi.weipaitang.com'],
                        ['workorder.weipaitang.com'],
                        ['cfc.weipaitang.com'],
                    ]
                },
                { name: 'ip', list: [] },
            ],
        },
        {
            name: 'yj',
            test: /(dev\.|t\.)?youjiang\.weipaitang\.com$/,
            envs: [
                { name: 'wanwan', hosts:['wanwan'], list: [] },
                {
                    name: 't',
                    list: [
                        ['api.weipaitang.com', 'apit.weipaitang.com'],
                        ['imapi.weipaitang.com', 'imapit.weipaitang.com'],
                        ['apiwanwan.weipaitang.com', 'apitestwanwan.weipaitang.com'],
                    ]
                },
                {
                    name: 'dev',
                    list: [
                        ['api.weipaitang.com', 'apidev.weipaitang.com'],
                        ['imapi.weipaitang.com', 'imapidev.weipaitang.com'],
                        ['apiwanwan.weipaitang.com', 'apidevwanwan.weipaitang.com'],
                    ]
                },
            ],
        },
    ],
    testEnv: [
        'env_01',
        'env_02',
        'env_03',
        'env_04',
        'env_05',
        'env_06',
        'env_07',
        'ns-test-01',
        'ns-test-02',
        'ns-test-03',
    ]
}
