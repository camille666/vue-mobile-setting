import { getState } from './store'
import config from './config' 
const { $ } = window

function urlParse(url = '') {
    if (!url) return;
    const [origin = '', search = ''] = url.split('?');
    const query = {};

    search.split('&').forEach(i => {
        const [key = '', value = ''] = i.split('=');
        query[key] = window.decodeURIComponent(value);
    });

    return {
        origin: origin.replace(/^\/\//, ''),
        query
    };
}

const ajaxToggle = (function(){
    if (!$ || !$.ajax) {
        throw new Error('window.$不存在，zepto可能未加载')
    }
    // ajax相关
    var _ajax = $.ajax;
    var ajax = {
        // 原始ajax
        origin: _ajax,
        mock(arg) {
            if (/\/compile/g.test(arg.url)) {
                return _ajax(...arg);
            }

            let { origin, query } = urlParse(arg.url);
            let xmlData = {};

            if (typeof arg.data === 'string') {
                xmlData = { data: arg.data, ...query };
            } else {
                xmlData = { ...query, ...arg.data };
            }

            var data = {
                ...arg,
                type: arg.type.toUpperCase() || 'GET',
                // 使用代理转发，解决不支持https问题
                url: `${config.mockApi}${origin}?project_name=PROJECT_NAME`,
                data: xmlData
            };
            return _ajax(data);
        },
    };

    return function ajaxToggle(dataSource) {
        const { ipHost, envs } = getState()

        switch (dataSource) {
            case 'mock': {
                $.ajax = ajax.mock
                break
            }
            default: {
                const result = envs.find(item => {
                    const { hosts = [], name } = item
                    return hosts.includes(dataSource) || name == dataSource
                })

                if (!result) {
                    // 使用默认请求
                    $.ajax = ajax.origin
                } else {
                    // 使用config内指定的代理匹配
                    $.ajax = function(options) {
                        const a = document.createElement('a')
                        a.href = options.url
                        result.list.some(item => {
                            if (item[0] === a.hostname) {
                                a.hostname = item[1] || item[0].replace(/(^[^.]+)/, '$1t')
                                options.url = a.href
                                return true
                            }
                            return false
                        })

                        return ajax.origin(options);
                    }
                }
            }
        }
    }
}())

export default ajaxToggle
