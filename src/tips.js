const tips = (function(){
    const STYLE = `
        position: fixed;
        z-index: 111112;
        top: 50%;
        left: 50%;
        -webkit-transform: translate3d(-50%, -50%, 0);
        transform: translate3d(-50%, -50%, 0);
        padding: 10px;
        background: rgba(0, 0, 0, .7);
        color: #fff;
        display: none;
        font-size: 14px;
        border-radius: 10px;
    `
    
    let div = document.createElement('div')
    let timeout = null

    div.style.cssText = STYLE
    document.body.appendChild(div)

    return function(content = '', time = 2000) {
        clearTimeout(timeout)
        div.textContent = content
        div.style.display = 'block'

        setTimeout(()=>{
            div.style.display = 'none'
        }, time)
    }
}())

export default tips
