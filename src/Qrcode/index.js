import initQrcode from './qrcode';

export function scan(fn) {
    window.wx && window.wx.scanQRCode && window.wx.scanQRCode({
        needResult: 1,
        desc: 'scanQRCode',
        success: fn,
        fail: (fail) => {
            console.warn({ fail });
        }
    });
}

let QRCode = null;

export function create(href, $mount) {
    !QRCode && (QRCode = initQrcode());

    new QRCode($mount, {
        width: document.body.clientWidth / 3,
        height: document.body.clientWidth / 3,
        text: href,
    });
}