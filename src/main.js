import Vue from 'vue'
import App from './index.vue'
import { getState } from './store'
import ajaxToggle from './ajaxToggle'
import config from './config'

const ID = 'proxy-proxy-proxy'
let div = document.createElement('div')
div.id = ID
document.body.appendChild(div)

new Vue({
    el: `#${ID}`,
    components: {App},
    template: `<App></App>`,
})

const reg = {
    get protocol() {
        return /^https?:\/{2}/
    }
}

;(function(){
    let {dataSource, proxyHost, proxyStatus} = getState()
    let proxyHostSource = proxyHost

    // 设置默认端口
    if (proxyHost && !proxyHost.includes(':')) {
        proxyHost += ':8000'
    }

    ajaxToggle(dataSource)

    const G = window[config.var] = window[config.var] || {}

    if (proxyStatus) {

        !proxyHost && alert('代理host不能为空')

        // ^https?\/{2} 以此开头，使用默认的
        if (reg.protocol.test(proxyHost)) {
            G.loadScriptHost = proxyHost
        } else {
            let tempProxyHost = `@${proxyHostSource}@`
            let proxyUrl = config.frpProxy

            // 去掉ip地址中的.，例如127.0.0.1 > 127001
            if (typeof parseFloat(proxyHostSource) === 'number') {
                tempProxyHost = tempProxyHost.replace(/\./g, '')
            }

            G.loadScriptHost = `${proxyUrl}${tempProxyHost}`
        }
        // 使用scriptEle.setAttribute('crossorigin', 'anonymous') 捕获跨域脚本错误
        G.loadScriptUseCrossorigin = true;
        // 有匠项目不使用manifest
        if (window.location.hostname.includes('youjiang.xin')) {
            return;
        }
        // 加载本地路由
        G.loadScriptHost && document.write(`<script src="${G.loadScriptHost}${config.routerPath}?v=${Date.now()}"><\/script> `);
    }
}())
