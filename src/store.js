import cookie from './clearCookie'
import config from './config'

const KEY = 'ly-proxy-data';
const isHttps = location.protocol.startsWith('https')

// 这些数据是会被存储在localStorage
const defaultData = {
    proxyHost: window.navigator.platform.match(/mac|win/i) ? `${location.protocol}//localhost:800${isHttps ? '1' : 0}` : config.JSHost, // js资源ip
    proxyStatus: false, // 是否开启js代理
    dataSource: location.hostname.split('.')[0], // 数据来源
    ipHost: '', // 后端ip地址
    href: '', // 方便无入口跳转
    visible: false, // 工具是否可见
    useVconsole: true, // 移动端使用vconsole
    showQrcode: false, // 二维码是否可见
}

// 有匠域名比较特殊
if (location.hostname == 'wt.youjiang.xin') {
    defaultData.dataSource = 't'
}

function getType(obj) {
    return Object.prototype.toString.call(obj).match(/\s+(.+)\]/)[1].toLowerCase()
}

function merge(base, data) {
    if (getType(data) !== 'object') {
        data = {}
    }
    Object.entries(data).forEach(([key, value]) => {
        if (value !== '' && value !== undefined) {
            base[key] = data[key]
        }
    })
    return base;
}

const initData = (function() {
    let data = {}
    try {
        data = JSON.parse(localStorage.getItem(KEY))
    } catch (err) {
        data = {}
    }

    const apiProxy = config.apiProxy.find(i => i.test.test(location.hostname))

    return {
        ...merge(defaultData, data),
        locationHref: location.href, // 当前url
        envs: [...apiProxy.envs, { name: 'mock' }],
        gitBranch: cookie.get('gitBranch'), // git branch
        grayCookie: cookie.get('wpt_debug'), // 是不是后端灰度
        isFrontGray: cookie.get('wpt_front_debug1') == 'true', // 是不是前端灰度
        wpt_env_num: cookie.get('wpt_env_num') || 'env_01', // 前测试环境
        wpt_payapi_debug: cookie.get('wpt_payapi_debug') || '', // 支付灰度
        testEnv: config.testEnv
    }
})();

export function getState() {
    return initData
}

export function setState(vueInstance) {
    const newData = {}
    Object.keys(defaultData).forEach(key => {
        newData[key] = initData[key]
    })
    localStorage.setItem(KEY, JSON.stringify(newData))
}
