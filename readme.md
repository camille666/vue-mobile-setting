# 前端开发小工具
![图片](https://cdn.weipaitang.com/static/20180523d9f2f234-40b5-46be-aec1-7d9b272bfe6d-W668H680)

小绿点的设计是，为了方便开发者在不同的开发环境切换

通过加载本地js路由文件复写服务器支出路由，以达到便捷开发的目的。
开发环境，直接访问本地静态服务。线上https环境，则通过内网https静态代理服务器加载本地文件。

除此之外，本插件还承担了开发环境自动切换api请求接口hostname的功能

## 使用
在index.php中引入
```php
<?php if(是开发人员) { ?>
    <script src="path/to/proxy.js"></script>
<? }endif ?>
```

## 功能实现

### 数据
通过劫持zepto的ajax函数，实现了请求域名更改

### 代理
通过更改WPT.localScriptHost，实现了js请求资源域名的更改


## 功能介绍
t/dev/灰度环境，默认都有小绿点。线上环境，则只有白名单用户、或者url search内含有fr=wpt_front_debug的页面上，才有小绿点。

### 数据
默认w环境请求w，t环境请求t，dev请求dev，ip请求不常见，mock是为了在开发前期使用
当然，你也可以在w环境，使用t环境的接口，但是仅仅用于那些不需要用户登录的接口。否则的话会弹出用户登录窗，因为t和w的用户系统是不一样的

### 代理
把js请求代理到指定ip地址。
我们的w.weipaitang.com、t.weipaitang.com域名会自动跳转到https协议，但本地的https证书不被浏览器信任。对于chrome浏览器，可以设置其信任localhost的https证书；手机浏览器，则只能使用代理服务来做请求的转发，我们使用的是https://proxy.wpt.la 这个域名。

需要注意的是，代理转发的请求时间要明显长于https://localhost:8001的请求，因此非必要条件下电脑上使用https://localhost:8001。

### 分支
基本不使用。
前期为了方便大家在不同的分支上开发，在dev环境发布后，可以访问到每个分支的代码。后面发现这种思路有问题，废弃之。

t为测试环境分支，gray为灰度环境分支。
大家都在各自的分支上开发，基本不会有冲突问题。那么代码合并到同一个分支上，然后走发布系统发布，是没有什么问题的。这样也避免了测试环境代码相互覆盖的问题。


### 入口
输入url，跳转到指定地址

### curl
显示当前url地址，点击url可更新到最新的url。点击curl则会复制url

### 前端灰度
所谓前端灰度，是js已经发布上线，但是路由文件并没有全量上线，只有页面上显示有小绿点的，才可以访问得到。
前端则是前端灰度环境。

### 后端灰度
前后端都在的灰度环境，前端通过【预发环境】发布gray分支

### reload
刷新页面

### 删除cookie
删除当前域名下的cookie

### 删除storage
清空localStorage sessionStorage

### vConsole
控制是否使用vConsole
手机上，因为没有了chrome devtool，可以通过vConsole看到console与请求。
PC模式建议关闭，因为vConsole的本质是劫持了console和XMLHttpRequest，在PC上我们当然是想看到原生的console与请求。


